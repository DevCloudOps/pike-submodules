#!/usr/bin/env python
from __future__ import print_function

import sys
if sys.version_info.major < 3: import six # allows python3 syntax in python2

from github import Github
import inspect, json, magic, pexpect, scandir, string

_ghub = None
class _gauth: accnt = 'honsys' ; pwd = 'jaked0gg'

_oslo = [ \
'oslo.cache', \
'oslo.concurrency', \
'oslo.config', \
'oslo.context', \
'oslo.db', \
'oslo.i18n', \
'oslo.log', \
'oslo.messaging', \
'oslo.middleware', \
'oslo.policy', \
'oslo.privsep', \
'oslo.reports', \
'oslo.rootwrap', \
'oslo.serialization', \
'oslo.service', \
'oslo.tools', \
'oslo.utils', \
'oslo.versionedobjects', \
'oslo.vmware' \
]

_subsys = [ \
'requirements', \
'keystone', \
'horizon', \
'glance', \
'cinder', \
'nova', \
'heat', \
'magnum', \
'neutron-lib', \
'neutron', \
'networking-generic-switch', \
'networking-odl', \
'networking-ovn', \
'python-openstacksdk', \
'python-openstackclient'
]

def glogin(auth=_gauth):
  global _ghub
  _ghub = Github(auth.accnt, auth.pwd)
  return _ghub

def glist_all(glist=[], org='openstack'):
  gos = _ghub.get_user('openstack')
  for repo in gos.get_repos():
    glist.append(repo.name) ; print(repo.name)

  return len(glist)

def gsubmodule(repos=_oslo, branch='stable/pike'):
  gitlab = 'git clone --recursive https://gitlab.com/DevCloudOps/pike-submodules.git'
  gsub = 'git submodule add -b ' + branch + ' '
  gurl = 'https://github.com/openstack/'
  print(gitlab, gsub, gurl)

  global _oslo
  for o in _oslo:
    cli = gsub + gurl + o + '.git' ; print(cli)
    pexpect.run(cli)

  global _subsys
  for s in _subsys:
    cli = gsub + gurl + s + '.git' ; print(cli)
    pexpect.run(cli)
# end gsubmodule

def list_files(path='./'):
  for entry in scandir(path):
    if not entry.name.startswith('.') and entry.is_file():
      print(entry.name)

if __name__ == '__main__':
  try:
    glogin()
  except:
    print('failed to login to GitHub')
    sys.exit()

  glist = []
# glen = glist_all(glist)    
  gsubmodule()



