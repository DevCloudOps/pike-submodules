Although these Rackspace blog items are a bit dated, much of the content remains relevant.

Refs.:

Github submodules:

https://github.com/blog/2104-working-with-submodules
https://git-scm.com/docs/git-submodule
Rackspace Blog July 2015: 

https://developer.rackspace.com/blog/install-openstack-from-source/ – general intro., external deps., keystone auth.
https://developer.rackspace.com/blog/install-openstack-from-source2/ – glance image service and neutron network controller
https://developer.rackspace.com/blog/install-openstack-from-source3/ – nova compute services controller
https://developer.rackspace.com/blog/install-openstack-from-source4/ – neutron network node (router?)
https://developer.rackspace.com/blog/install-openstack-from-source5/ – neutron agent(s) and nova compute node(s)
https://developer.rackspace.com/blog/install-openstack-from-source6/ – cinder storage and horizon dashboard 


Create an Openstack-Pike Git (bitbucket) repo. comprised of the essential elements used in the Cloud deployment of Openstack Pike:

Oslo (OpenStack common Lib mOdules)
Keystone service and client-cli
Glance services and client-cli
Neutron services, agents, and client-cli
Nova services and client-cli
Cinder and LVM services and client-cli
Horizon dashboard service and client-cli
Magnum (docker) container orchestration

TBD: list of oslo modules to support essential services and CLIs.

